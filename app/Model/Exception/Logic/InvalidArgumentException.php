<?php

namespace Model\Exception\Logic;

/**
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class InvalidStateException
 * @package Model\Exceptions\Logic
 */

class InvalidArgumentException extends \LogicException
{

}
