<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Entity;

/**
 * @property-read int $id m:schemaPrimary
 * @property      Department $department m:hasOne(department_id:department) m:schemaComment(Category joined to department)
 * @property      string $name m:schemaType(varchar:255) m:schemaComment(Name of Store)
 * @property      string $slug m:schemaType(varchar:255) m:schemaComment(Slug used for url)
 * @property      string|null $logoFilename m:schemaType(varchar:255)
 * @property      \DateTime|null $createDate m:schemaType(DateTime)
 * @property      Category|null $parentCategory m:hasOne(category_id:category) m:schemaComment(Category if category is under parent category)
 * @property      int $sort m:default(5000) m:schemaComment(Value for priority sorting)
 * @property      array|null $metadata m:passThru(jsonDecode|jsonEncode) m:schemaType(json)
 *
 * @schemaUnique slug
 */
class Category extends BaseEntity
{

}
