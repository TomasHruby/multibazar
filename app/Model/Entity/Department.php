<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Entity;

/**
 * @property-read int $id m:schemaPrimary
 * @property      string|null $domain m:schemaType(varchar:255) m:schemaComment(Department has own domain website)
 * @property      string $name m:schemaType(varchar:255) m:schemaComment(Name of Store)
 * @property      string $slug m:schemaType(varchar:255) m:schemaComment(Slug used for url)
 * @property      string|null $logoFilename m:schemaType(varchar:255)
 * @property      \DateTime|null $createDate m:schemaType(DateTime)
 * @property      array|null $metadata m:passThru(jsonDecode|jsonEncode) m:schemaType(json)
 *
 * @schemaUnique slug
 */
class Department extends BaseEntity
{

}
