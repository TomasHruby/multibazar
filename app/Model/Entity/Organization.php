<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Entity;

/**
 * @property-read int $id m:schemaPrimary
 * @property      string $name m:schemaType(varchar:255) m:schemaComment(Name of organization)
 * @property      string $slug m:schemaType(varchar:255) m:schemaComment(Slug used for url)
 * @property      string|null $contactEmail m:schemaType(varchar:255)
 * @property      string|null $contactPhone m:schemaType(varchar:31)
 * @property      string|null $website m:schemaType(varchar:255)
 * @property      string|null $regNumber m:schemaType(varchar:31)
 * @property      string|null $vatNumber m:schemaType(varchar:31)
 * @property      int|null $timezone m:schemaType(varchar:63)
 * @property      \DateTime|null $createDate m:schemaType(DateTime)
 * @property      array|null $metadata m:passThru(jsonDecode|jsonEncode) m:schemaType(json)
 */
class Organization extends BaseEntity
{

}
