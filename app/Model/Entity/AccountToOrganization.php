<?php
/*
* @author Tom Hruby
* https://tomashruby.com
*/

namespace Model\Entity;

/**
 * @property-read int           $id m:schemaPrimary
 * @property      Account       $account m:hasOne(account_id:account)
 * @property      Organization  $organization m:hasOne(organization_id:organization)
 * @property      int           $relationType m:schemaType(tinyint) m:enum(self::RELATION_TYPE_*) m:default(0)
 * @property      \DateTime|null $createDate m:schemaType(DateTime)
 *
 * @schemaUnique account_id, organization_id
 */
class AccountToOrganization extends BaseEntity
{

    const RELATION_TYPE_QUEST = 0;

    const RELATION_TYPE_OWNER = 1;

    const RELATION_TYPE_ADMIN = 2;

    const RELATION_TYPE_EDITOR = 3;

}
