<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Facade;

use Model\Entity\Account;
use Model\Exception\Runtime\EntityNotFound;
use Model\Repo\AccountRepo;
use Nette\Security\AuthenticationException;
use Nette\Security\Authenticator;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Security\BasicIdentity;
use Security\PermissionAuthorizator;

class AccountFacade
{

    /**
     * @var AccountRepo
     */
    protected AccountRepo $accountRepo;

    /**
     * @var Passwords
     */
    private $passwords;

    /**
     * @var PermissionAuthorizator
     */
    private $permission;

    /**
     * AccountFacade constructor.
     *
     * @param AccountRepo   $accountRepo
     * @param Passwords  $passwords
     * @param PermissionAuthorizator $permission
     */
    public function __construct(
        AccountRepo $accountRepo,
        Passwords $passwords,
        PermissionAuthorizator $permission
    ) {
        $this->accountRepo = $accountRepo;
        $this->passwords = $passwords;
        $this->permission = $permission;
    }

    /**
     * @param  string $email
     * @return Account
     * @throws EntityNotFound
     */
    public function getAccountByEmail(string $email): Account {
        /**
         * @var Account $user
         */
        $user = $this->accountRepo->getSingleBy(['email' => $email]);
        if (!$user) {
            throw new EntityNotFound('User not found!');
        }
        return $user;
    }

    /**
     * @param int $id
     * @return Account
     * @throws EntityNotFound
     */
    public function getAccountById(int $id): Account {
        $user = $this->accountRepo->getSingle($id);
        if (!$user) {
            throw new EntityNotFound('User not found!');
        }
        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @return IIdentity
     * @throws AuthenticationException
     */
    public function authenticate(string $email, string $password): IIdentity {
        try {
            $account = $this->getAccountByEmail($email);
        } catch (EntityNotFound $e) {
            throw new AuthenticationException('User not found: ' . $e->getMessage(), Authenticator::IDENTITY_NOT_FOUND);
        }

        if (!$account) {
            throw new AuthenticationException('User not found', Authenticator::IDENTITY_NOT_FOUND);
        } elseif (!$this->passwords->verify($password, $account->password)) {
            throw new AuthenticationException('The password is incorrect.', Authenticator::INVALID_CREDENTIAL);
        } elseif ($this->passwords->needsRehash($account->password)) {
            $account->password = $this->passwords->hash($password);
            $this->accountRepo->persist($account);
        }

        $identity = new BasicIdentity($account);

        return $identity;
    }

    public function passwordEncode(string $password): string {
        return $this->passwords->hash($password);
    }

}
