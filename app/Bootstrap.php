<?php

declare(strict_types=1);

namespace App;

use Nette\Bootstrap\Configurator;

define('APP_DIR', __DIR__);
define('ROOT_DIR', substr(__DIR__, 0, -4)); //app dir without "/app"
define('LOGS_DIR', ROOT_DIR . '/log');
define('TEMP_DIR', ROOT_DIR . '/temp');

class Bootstrap
{

    public static function boot(): Configurator {
        $configurator = new Configurator;
        $appDir = dirname(__DIR__);

        //$configurator->setDebugMode('secret@23.75.345.200'); // enable for your remote IP
        if (getenv('ENV')) {
            $configurator->setDebugMode(TRUE);
        }
        $configurator->enableTracy($appDir . '/log');

        $configurator->setTimeZone('Europe/Prague');
        $configurator->setTempDirectory($appDir . '/temp');

        $configurator->createRobotLoader()
            ->addDirectory(__DIR__)
            ->register();

        $configurator->addParameters(
            [
            'appDir' => APP_DIR,
            'rootDir' => ROOT_DIR,
            'logsDir' => LOGS_DIR,
            'tempDir' => TEMP_DIR,
            ]
        );

        $configurator->addConfig($appDir . '/config/common.neon');
        $configurator->addConfig($appDir . '/config/local.neon');

        return $configurator;
    }

}
