ProjectName
=================

Requirements
------------

- PHP 7.4


Migration with LeanMapper
------------

Follow the instructions 
https://www.janpecha.cz/blog/schemagenerator-generujte-svoji-databazi-z-entit/ or  
https://github.com/inlm/schema-generator/blob/master/docs/leanmapper-extractor.md

Create schema command:

	docker exec -it project_php-fpm php -f migrations/run/create.php

Update db command:

	docker exec -it project_php-fpm php -f migrations/run/update.php


Working dirs  `migrations/`.


Web Server Setup
----------------

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

	php -S localhost:8000 -t www

Then visit `http://localhost:8000` in your browser to see the welcome page.

For Apache or Nginx, setup a virtual host to point to the `www/` directory of the project and you
should be ready to go.

**It is CRITICAL that whole `app/`, `config/`, `log/` and `temp/` directories are not accessible directly
via a web browser. See [security warning](https://nette.org/security-warning).**



Console commands
----------------

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

	php bin/console logs:clean
